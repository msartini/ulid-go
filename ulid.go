package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/oklog/ulid"
)

type NumberReader struct {
	n int
}

func (r *NumberReader) Read(p []byte) (int, error) {
	// converter o número para string
	s := strconv.Itoa(r.n)
	// copiar a string para o slice de bytes
	copy(p, []byte(s))
	return len(s), nil
}

func main() {

	// t := time.Date(2022, 1, 1, 10, 4, 0, 0, time.UTC)

	// Gerar um novo ULID
	// id := ulid.MustNew(ulid.Timestamp(), nil)

	bytes := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}

	// gerar um número aleatório entre 0 e o comprimento do slice menos 1
	rand.Seed(time.Now().UnixNano())
	i := rand.Intn(len(bytes))

	// Passa um int para o NumberReader
	r := &NumberReader{n: i}

	// Imprime o valor do Reader a partir do rand do slice de 1 a 10
	// fmt.Println(r)

	// Gera o ULID a partir do timestamp atual e o Reader (no segundo parâmetro)
	id := ulid.MustNew(ulid.Timestamp(time.Now()), r)

	// id := ulid.MustNew(ulid.Timestamp(t), nil)

	// Imprimir o ULID como string
	fmt.Println(id.String())

	// id, err := ulid.Parse("01DVZ9J6D5W6KJGJHBF4JY3Z1")
	id, err := ulid.Parse(id.String())

	if err != nil {
		// Tratar o erro
		fmt.Println("Erro")
	}

}
